#include<iostream>
using namespace std;
class Acces{
    private:
    int priVar; 
    protected :
    int proVar;   
    public:
    int pubVar;
    void setVar(int priVar, int proVar, int pubVar){
        this->priVar = priVar;
        this->proVar = proVar;
        this->pubVar = pubVar;
    }
    int getVar(){  
        return priVar;
    }
    int getVars(){
        return proVar;
    }
};
int main(){
    Acces a;
    a.setVar(100,25,38);
    cout<<"Private var = "<<a.getVar()<<endl;
    cout<<"Protected var = "<<a.getVars()<<endl;
    cout<<"Public var = "<<a.pubVar<<endl;
    return 0;
}
