#include<iostream>
using namespace std;
class Base{
    public:
    int a = 140;
    protected :
    int b = 240;
    private:
    int c = 380;
    public:
    int getC(){
        return c;
    }
};
class A : public Base{
    public:
    int getB(){
        return b;
    }
};
class B : protected Base{
    public:
    int getA(){
        return  a;
    }
    int getB(){
        return b;
    }
};
class C : private Base{
    public:
    int getA(){
        return a;
    }
    int getB(){
        return b;
    }
};
int main(int argc, char const *argv[])
{
    cout<<"Public inheritance = "<<endl;
    A o;
    cout<<"Public var = "<<o.a<<endl;
    cout<<"Protected var = "<<o.getB()<<endl;
    cout<<"Private var = "<<o.getC()<<endl;
    B o2;
    cout<<"\nProtected inheritance = "<<endl;
    cout<<"Public var = "<<o2.getA()<<endl;
    cout<<"Protected var = "<<o2.getB()<<endl;
    cout<<"Private var cannot be accessed."<<endl;
    C o3;
    cout<<"\nPrivate inheritance = "<<endl;
    cout<<"Public var = "<<o3.getA()<<endl;
    cout<<"Protected var = "<<o3.getB()<<endl;
    cout<<"Private var cannot be accessed."<<endl;
    return 0;
}
