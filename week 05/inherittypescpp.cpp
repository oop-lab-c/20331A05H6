#include <iostream>
using namespace std;
class Vehicle {
public:
	Vehicle() { 
        cout << "This is a Vehicle\n"; 
        }
};
//simple inheritence
class fourWheeler : public Vehicle {
public:
	fourWheeler()
	{
		cout << "Objects with 4 wheels are vehicles\n";
	}
};
//multilevel inhritence
class Car : public fourWheeler {
public:
	Car() { 
        cout << "Car has 4 Wheels\n"; 
        }
};
//multiple inheritence
class fiveseater {
 public:
	fiveseater() { 
        cout << "This is a fiveseater\n"; 
    }
};
class jeep : public Car, public fiveseater {
	public:
	  jeep(){
	  cout << "jeep has 4 Wheels\n";
	} 
};
//hirarical inheritence
class top: public Vehicle {
public:
	top() { 
        cout << "it has a top\n"; 
        }
};

int main()
{
	Car obj;
	jeep obj2;
	top obj3;
	return 0;
}
