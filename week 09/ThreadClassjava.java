class A extends Thread{
    public void run(){
        for(int i=0;i<5;i++){
            System.out.println(i);
        }
    }
}
class B extends Thread{
    public void run(){
        for(int i=0;i<5;i++){
            System.out.println(i*2);
        }
    }
}
public class Main extends Thread{
    public static void main(String[] args){
        A obj1=new A();
        B obj2=new B();
        Main obj3=new Main();
        obj1.start();
        obj2.start();
        obj3.start();
        
    }
    public void run(){
           System.out.println("Thread is running");
      }
}